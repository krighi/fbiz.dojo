# Dojo F.biz

Reunião de todos os Dojos feitos na F.biz. Caso queira treinar algum problema ou melhorar alguma solução, siga os passos:

1. Baixe e instale o [GIT](https://git-scm.com/downloads), o [Nodejs](https://nodejs.org/) e o [Sublime Text](http://www.sublimetext.com/3);

1. Abra seu terminal (No windows, vá em Iniciar > Executar > escreva "cmd" > clique em OK):

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/cmd-1.png)

    que abrirá a linha de comando

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/cmd-2.png)

1. Instale o Jasmine com o comando `npm install -g jasmine`:

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/jasmine-1.png)    
    _No mac é provável que precise colocar `sudo` na frente e digitar sua senha_

1. Faça um fork deste repositório (necessário ter um usuário no [Bitbucket](https://bitbucket.org/account/signup/));

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/fork-1.png)

    e

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/fork-2.png)

1. Clone na sua máquina;
    
    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/clone-1.png)

    copie o comando e cole no seu terminal

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/clone-2.png)

1. O projeto estará na pasta do seu usuário. Abra a pasta no Sublime Text;

1. Para testar entre na pasta da data do dojo (ex.: `cd 2015-07-08/`) e execute `jasmine`;

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/jasmine-2.png)

1. Quando finalizar, atualize seu repositório com os comandos: `git commit -m "comentário da sua alteração"` e em seguida `git push` para enviar para o site do Bitbucket;

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/push-1.png)

1. Entre na aba "source", copie o link e poste sua solução na [nossa comunidade](http://discourse.fbiz.com.br/) para discutir e receber feedbacks.

    ![](https://bitbucket.org/fbiz/fbiz.dojo/raw/HEAD/_assets/share.png)
