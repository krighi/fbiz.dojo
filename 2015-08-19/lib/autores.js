module.exports = function(valor) {

	var nome = valor.split(' ');
	var sobrenome = nome.pop().toUpperCase();
	var palavrasReservadas = [
		"FILHO","FILHA","NETO",
		"NETA","SOBRINHO","SOBRINHA",
		"JUNIOR"
	];
	var palavrasLigacao = ["da", "de", "do", "das", "dos"];
	var isReservado = palavrasReservadas.indexOf( sobrenome ) != -1;
	var isPalavrasLigacao = palavrasLigacao.indexOf(nome[nome.length-1]) === -1;

	if( isReservado && nome.length > 1 && isPalavrasLigacao ){
		sobrenome = nome.pop().toUpperCase() + ' ' + sobrenome;
	}

	if (nome.length === 0) {
		return sobrenome;
	}

	nome = nome.map(function(item){
		item = item.toLowerCase();
		if (palavrasLigacao.indexOf(item) < 0){
			return item.charAt(0).toUpperCase() + item.slice(1);
		}
		return item;
	});

	return [
		sobrenome,
		nome.join(' ')
	].join(', ');
};
