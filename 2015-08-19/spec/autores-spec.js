describe('Nomes de autores', function(){

	var autores = require('../lib/autores.js');

	it('Nome composto com sobrenome comum', function() {
		expect(autores("Marcelo Miranda Carneiro"))
			.toBe("CARNEIRO, Marcelo Miranda");
		expect(autores("Marcelo Carneiro"))
			.toBe("CARNEIRO, Marcelo");
	});

	it('Nome composto com palavra reservada', function() {
		expect(autores("Marcelo Miranda Carneiro Neto"))
			.toBe("CARNEIRO NETO, Marcelo Miranda");
		expect(autores("Marcelo Miranda Carneiro Junior"))
			.toBe("CARNEIRO JUNIOR, Marcelo Miranda");
	});

	it('Nome bastardo', function() {
		expect(autores("Joao Neto"))
			.toBe("NETO, Joao");
	});

	it('Nome único', function() {
		expect(autores("Carneiro"))
			.toBe("CARNEIRO");
	});

	it('Nome minusculo', function() {
		expect(autores("marcelo carneiro"))
			.toBe("CARNEIRO, Marcelo");
	});

	it('Nome maiusculo', function() {
		expect(autores("MARCELO CARNEIRO"))
			.toBe("CARNEIRO, Marcelo");
	});

	it('Com do, de, das', function() {
		expect(autores("Marcelo Carneiro da Silva"))
			.toBe("SILVA, Marcelo Carneiro da");
		expect(autores("Marcelo de Neto"))
			.toBe("NETO, Marcelo de");
	});


	it('Nome minusculo e palavra reservada', function() {
		expect(autores("marcelo carneiro sobrinho"))
			.toBe("CARNEIRO SOBRINHO, Marcelo");
	});

	it('Nome minusculo e palavra reservada', function() {
		expect(autores("Marcelo Filho de Alcantara Neto"))
			.toBe("ALCANTARA NETO, Marcelo Filho de");
	});
});
