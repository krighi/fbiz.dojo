module.exports = function(valor) {

	var numeros = [
		undefined,
		[" ", "|", "|"].join('\n'),
		[" _ ", " _|", "|_ "].join('\n'),
		["_ ", "_|", "_|"].join('\n'),
		["   ", "|_|", "  |"].join('\n'),
		[" _ ", "|_ ", " _|"].join('\n'),
		[" _ ", "|_ ", "|_|"].join('\n'),
		["_ ", " |", " |"].join('\n'),
		[" _ ", "|_|", "|_|"].join('\n'),
		[" _ ", "|_|", " _|"].join('\n')
	];
	return {
		numeros: numeros,
		getCompoundNumber: function(valor) {

			valor = valor.split('\n');
			var numberList = [];
			var linha = valor[0].split('');
			var spaces = [];

			linha.forEach(function(val, index) {
				var is_space = true;
				for( var i = 0; i< 4; i++) {
					if(val[i] != ' '){
						is_space = false;
					}
				};
				if( is_space ){

				}
			});
			return 123;
		},
		getNumber: function(valor) {
			if (valor.length > 11) {
				return this.getCompoundNumber(valor);
			}
			var index = numeros.indexOf(valor);
			return index < 0 ? false : index;
		}
	};
};
