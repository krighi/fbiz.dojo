describe('Título do teste', function(){

	var fn = require('../lib/fn.js')();
	var numeros = [
		undefined,
		[" ", "|", "|"].join('\n'),
		[" _ ", " _|", "|_ "].join('\n'),
		["_ ", "_|", "_|"].join('\n'),
		["   ", "|_|", "  |"].join('\n'),
		[" _ ", "|_ ", " _|"].join('\n'),
		[" _ ", "|_ ", "|_|"].join('\n'),
		["_ ", " |", " |"].join('\n'),
		[" _ ", "|_|", "|_|"].join('\n'),
		[" _ ", "|_|", " _|"].join('\n')
	];
	var numero_composto = [
		'     _   _  ',
		'  |  _|  _| ',
		'  | |_   _| ',
		'            '
	].join('\n')


	it('Teste do numeros' , function() {
		expect( fn.getNumber(numeros[1]) ).toBe(1);
		expect( fn.getNumber(numeros[2]) ).toBe(2);
		expect( fn.getNumber(numeros[3]) ).toBe(3);
		expect( fn.getNumber(numeros[4]) ).toBe(4);
		expect( fn.getNumber(numeros[5]) ).toBe(5);
		expect( fn.getNumber(numeros[6]) ).toBe(6);
		expect( fn.getNumber(numeros[7]) ).toBe(7);
		expect( fn.getNumber(numeros[8]) ).toBe(8);
		expect( fn.getNumber(numeros[9]) ).toBe(9);
	});

	it('Teste do numero composto' , function() {
		expect( fn.getNumber(numero_composto)).toBe(123);
	});
	it('Teste falha' , function() {
		expect( fn.getNumber("") ).toBe(false);
	});

});
