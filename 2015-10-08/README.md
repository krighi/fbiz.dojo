# Dojo - 08/10/2015

Dojo realizado na F.biz, feito em JS.

## Problema - OCR Bancário

Você trabalha para um banco, que recentemente comprou uma máquina muito engenhosa para auxiliar na leitura de cartas e faxes enviados para o escritório-central. Esta máquina escaneia os documentos em papel e produz um arquivo com um grande número de entradas, sendo que cada uma tem este formato:

       _  _       _   _  _   _   _
    |  _| _| |_| |_  |_   | |_| |_|
    | |_  _|   |  _| |_|  | |_|  _|

Cada entrada possui 4 linhas, e cada linha possui 27 caracteres. As 3 primeiras linhas contém o número da conta, utilizando pipes e underscores, e a quarta linha é vazia. Cada número de conta possui nove dígitos (entre 0 e 9). Cada arquivo pode conter até 500 registros. Sua tarefa é desenvolver um programa que obtenha esse arquivo e devolva a lista de contas.

Retirado de [http://dojopuzzles.com/problemas/exibe/ocr-bancario/](http://dojopuzzles.com/problemas/exibe/ocr-bancario/)

## Participantes

* Adriano Enache
* Caio Liberali
* Eduardo Aragão
* Kauê Righi
* Marcelo Carneiro
* Nicholas Almeida
* Renato Inamine

## :)

* Foi engraçado;
* Discussão sobre TDD;
* A gente aprender que esse tipo de problema é muito complicado pra 1h30;
* Todo mundo contribuiu com o código — participação ativa de todos;
* Presença de 7 participantes;

## :(

* Não conseguimos finalizar nem metade do problema;
* Problema muito complicado;
* Enunciado do problema muito confuso;
* Não teve café;
* 7 minutos por rodada pareceu muita coisa pro piloto;
* Horário do dojo muito negativo pra maioria;
* Algumas pessoas não chegaram no horário;

## Melhorar

* As pessoas poderiam ter mandado mais problemas;
