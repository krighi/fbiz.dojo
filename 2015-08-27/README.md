# Dojo - 27/08/2015

Dojo realizado na F.biz, feito em JS.

## Problema - Justificar texto

Description:

Your task in this Kata is to emulate text justification in monospace font. You will be given a single-lined text and the expected justification width. The longest word will never be greater than this width.

Here are the rules:

Use spaces to fill in the gaps between words.
Each line should contain as many words as possible.
Use '\n' to separate lines.
Gap between words can't differ by more than one space.
Lines should end with a word not a space.
'\n' is not included in the length of a line.
Large gaps go first, then smaller ones: 'Lorem---ipsum---dolor--sit--amet' (3, 3, 2, 2 spaces).
Last line should not be justified, use only one space between words.
Last line should not contain '\n'
Strings with one word do not need gaps ('somelongword\n').
Example with width=30:

Retirado de [http://www.codewars.com/kata/text-align-justify](http://www.codewars.com/kata/text-align-justify)

## Participantes

* Igor Almeida
* Adriano Enache
* Carlos Rossignatti
* Kaue Righi
* Marcelo Viera
* Renato Inamine

## :)

* Cafe no chegou no horário;
* Problema mais dificil;
* Foi legal ver as várias formas de resolver o problema e chegar num acordo em como resolver o problema

## :(

* Faltou primeiro definir os steps do teste;
* Dificil trabalhar com testes de output visual;
* Não conseguimos termiar nem refatorar;

## Melhorar

* Respeitar os babe-steps;
