module.exports = function(lorem, columns) {

	var palavras = lorem.split(' ');
	var totalCaracteres = 0;
	var linha = [];
	var linhas = [];

	palavras.forEach(function(palavra, index){
		if (linha.length === 0){
			totalCaracteres += palavra.length;	
		} else {
			totalCaracteres += palavra.length+1;
		}
		if (totalCaracteres <= columns) {
			linha.push(linha.length ? " " + palavra : palavra );
		} else {
			totalCaracteres -= palavra.length + 1;
			console.log(linhas.length + " tem chars: " + totalCaracteres);
        	linhas.push(linha);
        	linha = [palavra];
        	totalCaracteres = palavra.length;
		}
	});

	//PAROU AQUI
	//linhas.map();

	function justificaTexto (linha, totalCaracteres) {
		var diferencaEspacos = columns - totalCaracteres;
		console.log("precisa justificar " + diferencaEspacos + " espacos");
		return linha;
	    
		var indice = 0;
		var palavra;
		while(diferencaEspacos--) {
			palavra = linha[indice];
			palavra += " "; 
			linha[indice] = palavra;
			indice ++;
			if (indice >= linha.length) {
				indice = 0;
			}
		}
		return linha;
	}
 
	var textoRetorno = "";
	linhas.forEach(function(arrLinha){
		textoRetorno += arrLinha.join("") + "\n";
	});

	return textoRetorno;
};
