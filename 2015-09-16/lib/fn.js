module.exports = function(tempoRodada, totalTurnos) {
	tempoRodada = tempoRodada * 1000;
	var exec = require('child_process').exec;
	var rodada = 0;
	var intervalo = 10 * 1000;
	var gameStart = new Date();
	var total = Number(gameStart) + (totalTurnos * tempoRodada) + (intervalo * (totalTurnos - 1));
	var rodadaStart = gameStart;
	console.log((new Date(new Date(total) - gameStart)).toUTCString());

	var contador = 0;
	return {
		isRodada: function( start, end ) {
			return ((Number(end) - Number(start)) == tempoRodada);
		},
		isFinalJogo: function(turno){
			return turno >= totalTurnos;
		},
		tick:function() {
			var now = Number(gameStart) + (++contador * 1000);
			process.stdout.write('\033c');
			console.info(this.showRelogio(new Date(total - now)));

			if (this.isRodada(rodadaStart, now)) {
				rodadaStart = now + intervalo;
				rodada++;

				if (this.isFinalJogo(rodada)) {
					exec('say -v Joana "Então pronto! Acabaste o jogo!"');
					return false;
				}

				exec('say -v Joana "Acabou a rodada, troquem as cadeiras pois!"');
			}
			return true;
		},
		getTotalRodadas:function(){
			return rodada;
		},
		showRelogio : function(tempo){
			function leftZero(val) {
				return val < 10 ? '0' + val : val;
			}

			return [
				leftZero(tempo.getUTCHours()),
				leftZero(tempo.getUTCMinutes()),
				leftZero(tempo.getUTCSeconds())
			].join(':');
		}
	}
};
