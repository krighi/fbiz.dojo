describe('Título do teste', function(){

	var fn = require('../lib/fn.js');

	var now = new Date();

	it('final de rodada', function() {
		var timer = fn(10);
		expect(timer.isRodada(now - 10*1000, now))
			.toBe(true);
	});

	it('não é final de rodada', function() {
		var timer = fn(10);
		expect(timer.isRodada(now - 10*1000, now - 1000))
			.toBe(false);
		expect(timer.isRodada(now - 10*1000, now + 1000))
			.toBe(false);
	});

	it('final de jogo', function() {
		var timer = fn(10, 5);
		expect(timer.isFinalJogo(1))
			.toBe(false);
		expect(timer.isFinalJogo(5))
			.toBe(true);
	});

	it('final de jogo apos 5 rodadas', function() {
		var timer = fn(10, 5);
		for (var i = 1; i <= 100; i++) {
			timer.tick();
		}
		expect(timer.isFinalJogo(timer.getTotalRodadas()))
			.toBe(true);
	});
	it('relogio precisa mostrar o tempo no formato humano', function() {
		var timer = fn(10, 5);
		expect(timer.showRelogio(new Date(2015,00,01,01,00,00))).toBe("01:00:00");
	});

});
