# Dojo - 13/08/2015

Dojo realizado na F.biz, feito em JS.

## Problema - Caixa eletrônico

Desenvolva um programa que simule a entrega de notas quando um cliente efetuar um saque em um caixa eletrônico. Os requisitos básicos são os seguintes:

* Entregar o menor número de notas;
* É possível sacar o valor solicitado com as notas disponíveis;
* Saldo do cliente infinito;
* Quantidade de notas infinito (pode-se colocar um valor finito de cédulas para aumentar a dificuldade do problema);

Notas disponíveis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00

> Exemplos:
> 
> * Valor do Saque: R$ 30,00 – Resultado Esperado: Entregar 1 nota de R$20,00 e 1 nota de R$ 10,00.
> * Valor do Saque: R$ 80,00 – Resultado Esperado: Entregar 1 nota de R$50,00 1 nota de R$ 20,00 e 1 nota de R$ 10,00.

Retirado de [http://dojopuzzles.com/problemas/exibe/caixa-eletronico/](http://dojopuzzles.com/problemas/exibe/caixa-eletronico/)

## Participantes

* Adriano Enache
* Eduardo Aragão
* Igor Almeida
* Luiz Bárbara
* Marcelo Carneiro
* Marcelo Vieira
* Nadia Araújo
* Renato Inamine

## :)

* Primeiro dojo que conseguimos resolver e refatorar um pouco;
* Todos jogaram pelo menos 2x;
* Seguimos o enunciado à risca;
* Problema foi um desafio que se encaixou bem ao exercício;
* Pessoal mais focado e com menos interrupção;
* Conseguimos seguir mais as regras do jogo;
* Horário e tempo de duração;

## :(

* Co-piloto não foi muito ativo;
* As vezes fomos um pouco afobados ao dar sugestões no momento em que não éramos co-piloto;
* Pessoal não explicita muito a necessidade da ajuda;
* Não teve café;

## Melhorar

* Café da manhã;
* Testar um horário à noite;
