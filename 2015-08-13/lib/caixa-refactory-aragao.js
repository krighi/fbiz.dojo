module.exports = function( valor ){

	var notas = [ 100, 50, 20, 10 ],
		resultado = [];

	notas.forEach(function( nota, i ){
		while( valor >= nota ){
			valor = valor - nota;
			resultado.push(nota);
		}
	});

	if (valor !== 0) {
		return 'erro';
	}

	return resultado.join('-');
};
